#!/usr/bin/env python
# **- encoding: utf-8 -**
"""
    Minido-Unleashed is a set of programs to control a home automation
    system based on minido from AnB S.A.

    Please check http://kenai.com/projects/minido-unleashed/

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    ***

    This program connects to a STOMP server, and allow dual way communication
    with the minido bus, checking the validity of the packet before sending.
"""

###############################################################################
from __future__ import print_function
from twisted.application import internet, service
from twisted.internet.protocol import Protocol, ReconnectingClientFactory
from twisted.internet.protocol import Factory


# Other imports
import datetime
import time
import sys
import msgpack

# minido
from protocol import *

# txZMQ
from txzmq import ZmqFactory, ZmqEndpoint, ZmqPubConnection, ZmqSubConnection

MINIDO_LISTEN_HOST = 'localhost'
MINIDO_LISTEN_PORT = 2323

class MinidoServerFactory(Factory):
    def __init__(self):
        self.connections = []

    def startedConnecting(self, connector):
        print('Started to connect.')

    def buildProtocol(self, addr):
        return MinidoProtocol(self)

    def clientConnectionLost(self, connector, reason):
        print('Lost connection.  Reason:', reason)
        ReconnectingClientFactory.clientConnectionLost(self, connector, reason)

    def clientConnectionFailed(self, connector, reason):
        print('Connection failed. Reason:', reason)
        ReconnectingClientFactory.clientConnectionFailed(self, connector,
                                                         reason)
    def recv_message(self, message):
        print(': TCP to STOMP : %s' % (' '.join(map(lambda i: '{0:02X}'.format(i),message))))
        pubconn.publish(msgpack.packb(message), '')
 
if __name__ == '__main__':
    from twisted.internet import reactor
    zf = ZmqFactory()
    minidoFactory = MinidoServerFactory()
    sube = ZmqEndpoint('connect', 'tcp://192.168.232.103:5557')
    pube = ZmqEndpoint('connect', 'tcp://192.168.232.103:5558')
    subconn = ZmqSubConnection(zf, sube)
    subconn.subscribe('')

    def send2tcp(rawmessage, tag):
        message = msgpack.unpackb(rawmessage)
        if type(message) is tuple:
            print(": STOMP to %i TCP client(s) : %s" % (
                len(minidoFactory.connections), 
                ' '.join(map(lambda i: '{0:02X}'.format(i),message))))
            for conn in minidoFactory.connections:
                conn.send_data(message)

    pubconn = ZmqPubConnection(zf, pube)
    minidoFactory.pubconn = pubconn
    subconn.gotMessage = send2tcp
    reactor.listenTCP(MINIDO_LISTEN_PORT, minidoFactory)
    reactor.run()


