python3 -mvenv venv
. venv/bin/activate
pip install --upgrade -r requirements.txt
echo " "
echo "=================="
echo "Make sure to run :"
echo "=================="
echo ". venv/bin/activate"
echo "in your console to activate the python virtual environment"
